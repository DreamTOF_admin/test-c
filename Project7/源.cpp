#define _CRT_SECURE_NO_WARNINGS
//根据实际处理数据的类型定义链队中结点的值域类型elemtype
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
typedef int elemtype;
typedef  struct node		    //队列结点类型定义
{
	elemtype data; 		    //队列的数据元素类型
	struct node* next; 		//指向后继结点的指针
}NODE;
typedef struct
{	                  //定义链队
	NODE* front, * rear;//定义链队队头和队尾指针
}LINKQUEUE;

void initqueue(LINKQUEUE* ABC)//队列的初始化
{
	ABC->front = (NODE*)malloc(sizeof(NODE));//队列为带头结点的链队列
	ABC->front->next = NULL;
	ABC->rear = ABC->front;
}
LINKQUEUE* pushqueue(LINKQUEUE* ABC, elemtype x)
{ //将元素x插入到链队列ABC中，作为ABC的新队尾
	NODE* a;
	a = (NODE*)malloc(sizeof(NODE));
	if (a == NULL)
	{
		printf("申请空间失败");
		return 0;
	}
	a->data = x;
	a->next = NULL;
	ABC->rear->next = a;
	ABC->rear = a;
	return ABC;
}
elemtype popqueue(LINKQUEUE* ABC)
{  //若链队列不为空，则删除队头元素，返回其元素值
	NODE* a;
	if (ABC->front == ABC->rear)
	{
		printf("队列为空,出队失败");
		return 0;
	}
	else
	{
		a = ABC->front->next;
		ABC->front->next = a->next;
		free(a);
	}

}

void printqueue(LINKQUEUE* ABC)//队列的显示
{
	NODE* a;
	a = ABC->front->next;
	if (a == NULL)
		printf("队列空!");
	while (a != NULL)
	{
		if (a->next == NULL)
			printf("%d", a->data);
		else
			printf("%d<--", a->data);
		a = a->next;
	}
	printf("\n");
}
void main()
{
	LINKQUEUE* a;
	int choice, elemdata, x = 0;
	a = (LINKQUEUE*)malloc(sizeof(LINKQUEUE));
	initqueue(a);
	while (1)
	{
		printf("      欢迎使用队列操作小程序：\n");
		printf("\t1、元素入队\n");
		printf("\t2、元素出队\n");
		printf("\t3、显示队列\n");
		printf("\t4、清屏幕\n");
		printf("\t5、退出程序\n");
		printf("      请选择你的操作：");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1:printf("请输入进队元素：");
			scanf("%d", &elemdata);
			a = pushqueue(a, elemdata);
			printf("队列中的元素为：\n");
			printqueue(a);
			system("pause");
			break;
		case 2:x = popqueue(a);
			if (x != 0)
				printf("元素%d出队!\n", x);
			printf("队列中的元素为：\n");
			printqueue(a);
			system("pause");
			break;
		case 3:printf("队列中的元素分别为：\n");
			printqueue(a);
			system("pause");
			break;
		case 4:system("cls");
			break;
		case 5:return;
		}
		system("cls");
	}
}

